#!/bin/bash

conn=$(echo -n "{query}" | sed 's/\\//g')
osascript -e 'tell application "Viscosity" to disconnectall'
osascript -e "tell application \"Viscosity\" to connect \"$conn\""
